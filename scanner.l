/*
  Praise the Sun
    -Adriano Soares
    -Luiza Eitelvein
    -Vinicius Herbstrith
*/
%option  nodefault

%x comentario
%{
#include "misc.h"
#include "main.h"
#include "parser.h" //arquivo automaticamente gerado pelo bison


int linha = 1;
int type;
comp_stack_t* stack;
comp_dict_t* table;
comp_dict_item_t data;
%}

%%

[\ \s\t]     {          }
<*>\n        { linha++; }
\/\/.*\n     { linha++; }

\/\*                      BEGIN(comentario);
<comentario>[^*\n]*       {           }
<comentario>"*"+[^*/\n]*  {           }
<comentario>.*\*\/        BEGIN(INITIAL);


int      { yylval.type = IKS_INT;    return TK_PR_INT;    }
float    { yylval.type = IKS_FLOAT;  return TK_PR_FLOAT;  }
char     { yylval.type = IKS_CHAR;   return TK_PR_CHAR;   }
string   { yylval.type = IKS_STRING; return TK_PR_STRING; }
bool     { yylval.type = IKS_BOOL;   return TK_PR_BOOL;   }
if       { return TK_PR_IF;     }
then     { return TK_PR_THEN;   }
else     { return TK_PR_ELSE;   }
while    { return TK_PR_WHILE;  }
do       { return TK_PR_DO;     }
input    { return TK_PR_INPUT;  }
output   { return TK_PR_OUTPUT; }
return   { return TK_PR_RETURN; }

[\,\;\:\(\)\[\]\+\-\*\/\<\>\=\!\&\$] { return yytext[0]; }

\{                {
                    //table = push(stack, create_hash_table(1024));
                    return yytext[0];
                  }
  
\}                {
                    //dump_table(table);
                    //table = pop(stack);
                    return yytext[0];
                  }

"<="	          { return TK_OC_LE;  }
">="	          { return TK_OC_GE;  }
"=="	          { return TK_OC_EQ;  }
"!="	          { return TK_OC_NE;  }
"&&"	          { return TK_OC_AND; }
"\|\|"	          { return TK_OC_OR;  }

[0-9]+          { data.line = linha; 
                  data.type = IKS_SIMBOLO_LITERAL_INT ;
                  data.value.int_data = atoi(yytext);
                  yylval.symbol = add_data(table, yytext, data); 
                  return TK_LIT_INT;	}
[0-9]+\.[0-9]+  { data.line = linha; 
                  data.type = IKS_SIMBOLO_LITERAL_FLOAT;
                  data.value.float_data = atof(yytext);
                  yylval.symbol = add_data(table, yytext, data); 
                  return TK_LIT_FLOAT;  }
false           { data.line = linha; 
                  data.type = IKS_SIMBOLO_LITERAL_BOOL;
                  data.value.int_data = 0;
                  yylval.symbol = add_data(table, yytext, data); 
                  return TK_LIT_FALSE; } 
true            { data.line = linha; 
                  data.type = IKS_SIMBOLO_LITERAL_BOOL;
                  data.value.int_data = 1;
                  yylval.symbol = add_data(table, yytext, data); 
                  return TK_LIT_TRUE;   }
\'.\'           { data.line = linha;
                  data.type = IKS_SIMBOLO_LITERAL_CHAR; 
                  char *str = malloc(2);
                  str[0] = yytext[1]; str[1] = (char)0;
                  data.value.char_data = str[0];  
                  yylval.symbol = add_data(table, str, data); 
                  free(str);
                  return TK_LIT_CHAR;   }
\"[^\"]*\"      { data.line = linha;
                  data.type = IKS_SIMBOLO_LITERAL_STRING; 
                  char *str = malloc(strlen(yytext)-1);
                  strncpy(str, &(yytext[1]), strlen(yytext)-2); 
                  str[strlen(yytext)-2] = (char)0;
                  data.value.string_data = str;
                  yylval.symbol = add_data(table, str, data);
                  //free(str);
                  return TK_LIT_STRING; }

[a-zA-Z_][a-zA-Z0-9_]* {                         
                        //yylval.scanner_data = data;
                        //yylval.line = linha;
                        //yylval.type = IKS_SIMBOLO_IDENTIFICADOR;
                        //type = IKS_SIMBOLO_IDENTIFICADOR;
                        yylval.str = yytext;
                        //yylval.symbol = add_data(table, yytext, data); 
                        return TK_IDENTIFICADOR; }

. 					   { return TOKEN_ERRO; }

%%
