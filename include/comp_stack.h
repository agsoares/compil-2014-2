#ifndef __COMP_STACK_H
#define __COMP_STACK_H

#include "comp_dict.h"
#include "comp_tree.h"
#include "main.h"

typedef struct return_list_t {
  comp_tree_t* node;
  struct return_list_t* next;
} return_list_t; 

typedef struct comp_stack_entry_t
{
  comp_dict_t *value;
  int memory_position_counter;
  struct return_list_t* return_list;
  struct arg_list_t* args_list;
  struct comp_stack_entry_t *prev;
  struct comp_stack_entry_t *next;
} comp_stack_entry_t;

typedef struct comp_stack_t
{
  comp_stack_entry_t *bottom;
  comp_stack_entry_t *top;
} comp_stack_t;

comp_stack_t* create_stack();
comp_dict_t* push(comp_stack_t *stack, comp_dict_t *value);
comp_dict_t* pop(comp_stack_t *stack);
comp_dict_t* peek();
comp_dict_entry_t* search_for_id_stack(comp_stack_t *stack, char* key);
#endif
