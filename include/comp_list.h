#ifndef __COMP_LIST_H
#define __COMP_LIST_H

#include <stdio.h>
#include <stdlib.h>

typedef struct tac_struct
{
  char* op1;
  char* op2;
  char* op3;
  char* opcode;
  char* label;
} TAC;

typedef struct comp_list_t
{
  struct comp_list_item_t *first;
  struct comp_list_item_t *last;
} comp_list_t;

typedef struct comp_list_item_t
{
  char *instruction;
  TAC *inst_tac;
  struct comp_list_item_t *next;
} comp_list_item_t;





comp_list_t* create_list();
comp_list_t* concat(comp_list_t* listA, comp_list_t* listB);
comp_list_t* append(comp_list_t* list, char* instruction);
comp_list_t* prepend(comp_list_t* list, char* instruction);
void print_list (comp_list_t* list);
#endif
