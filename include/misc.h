#ifndef __MISC_H
#define __MISC_H
#include <stdio.h>
#include <stdarg.h>
#include "comp_dict.h"
#include "comp_tree.h"
#include "comp_stack.h"

extern int linha;
extern comp_dict_t* table;
extern comp_tree_t* tree;
extern comp_tree_list_t* tree_list;
extern comp_stack_t* stack;

int getLineNumber (void);
void yyerror (char const *mensagem);
void main_init (int argc, char **argv);
void main_finalize (void);
reg_list *reverse(reg_list *list);

char* get_register();
char* get_label();
char* create_instruction(char* opcode, int n, ...);
char* set_label(char *label);
char* create_instruction_generic(char* opcode, int n, ...);
#endif
