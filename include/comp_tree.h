#ifndef __COMP_TREE_H
#define __COMP_TREE_H

#include <stdarg.h>
#include <stdlib.h>
#include "iks_ast.h"
#include "comp_dict.h"
#include "comp_list.h"
#include "main.h"

typedef struct reg_list
{
  char *reg;
  struct reg_list *next;

}reg_list;


typedef struct comp_tree_t
{
  //defined on iks_ast.h  
  int type;
  int coercion_type;	// 0 if don't needs coercion
  int iks_type;
  struct comp_dict_entry_t *symbol_table_entry;
  struct comp_dict_t *symbol_table;
  int number_of_childs;
  struct comp_tree_t **childs;
  char *label;
  char *temp;
  comp_list_t *code; 
  reg_list *temp_list;
  
} comp_tree_t;





typedef struct comp_tree_list_t
{
  struct comp_tree_t *node;
  struct comp_tree_list_t *next;
} comp_tree_list_t;





                  

comp_tree_t* create_node(int nodeType, comp_dict_entry_t *symbol_table_node,int num_childs, comp_tree_t* son, ...);
comp_tree_t* add_child(comp_tree_t* parent, comp_tree_t* child);
void free_node(comp_tree_t* node);
int compare_node_and_args(comp_tree_t* parent, arg_list_t* args);
#endif

