#ifndef __COMP_DICT_H
#define __COMP_DICT_H

#include <stdlib.h>
#include <string.h>

typedef struct arg_list_t {
  struct comp_dict_entry_t* symbol;
  struct arg_list_t* next;
} arg_list_t; 

union TypeValue
{
  int int_data;
  float float_data;
  char  char_data;
  char* string_data;
  int teste;
}typevalue;

typedef struct comp_dict_item_t
{
  int line;
  int type;
  int id_type;    //0 = not id, 1 = simple id, 2 = array id, 3 = function id
  int size;    // number of bytes the variable uses
  int memory_position;
  int global; //1 if global variable
  int array_indexes_num;
  int array_indexes_size[20];
  union TypeValue value;
} comp_dict_item_t;


typedef struct comp_dict_entry_t {
    comp_dict_item_t data;
    char* key;
    int type;
    
    arg_list_t* args_list;  
    struct comp_dict_entry_t *next;
} comp_dict_entry_t;



typedef struct comp_dict_t {
    int size;
    comp_dict_entry_t **table; /* the table elements */
} comp_dict_t;

comp_dict_t *create_hash_table(int size);
unsigned int hash(comp_dict_t* hashtable, char* key);
comp_dict_entry_t* lookup_key(comp_dict_t* hashtable, char* key, int type);
comp_dict_entry_t* add_data(comp_dict_t* hashtable, char* key, comp_dict_item_t data);
/*
int compare_arguments(argument_t *function_args, argument_t *call_args);
void add_argument_to_list(argument_t *args_list, int new_argument_type);
void dump_args(argument_t *args_list);
*/
void dump_table(comp_dict_t* hashtable);
#endif
