#include "comp_tree.h"
#include "comp_list.h"
#include <stdio.h>
#include "misc.h"

comp_tree_t* create_node(int nodeType, comp_dict_entry_t *symbol_table_node,int num_childs, comp_tree_t* son, ...) {
  comp_tree_t *new_node;
  if ((new_node = malloc(sizeof(comp_tree_t))) == 0) 
    return NULL;
  
  comp_tree_t **child_array;
  if ((child_array = malloc(sizeof(comp_tree_t*) * 4)) == 0) 
    return NULL;
  
  new_node->type = nodeType;
  new_node->coercion_type = 0;
  new_node->symbol_table_entry = symbol_table_node;
  new_node->number_of_childs = num_childs;
  
  
  if (num_childs > 0) {
    va_list arg_list;
    
    va_start(arg_list, son);
    comp_tree_t *temp;

    int i;
    for (temp = son, i = 0; i < num_childs; temp = va_arg(arg_list, comp_tree_t*), i++) {
      child_array[i] = temp;
    }
    va_end(arg_list);
  }
  new_node->childs = child_array;
  comp_tree_list_t *tree_node = malloc(sizeof(comp_tree_list_t));
  tree_node->node = new_node;
  tree_node->next = tree_list;
  tree_list = tree_node;
  new_node->code = create_list();
  return new_node;
}

comp_tree_t* add_child(comp_tree_t* parent, comp_tree_t* child) {
  parent->childs[parent->number_of_childs] = child;
  parent->number_of_childs++;
  return parent;
}

//return errors or 1 if they are equal
int compare_node_and_args(comp_tree_t* parent, arg_list_t* args)
{
  comp_tree_t* son_temp = parent; 
  arg_list_t* arg_temp = args;
  
  int i =0;
  while(arg_temp != NULL)
  {
    if(son_temp == NULL)
    { 
      return IKS_ERROR_MISSING_ARGS;
    }
     
    if(son_temp->iks_type != arg_temp->symbol->data.type)
    {  
      return IKS_ERROR_WRONG_TYPE_ARGS;
    }
    i++;
    son_temp = parent->childs[0]; 
    arg_temp = arg_temp->next;
  }
  
  if(son_temp != NULL)
  {  
    return IKS_ERROR_EXCESS_ARGS;
  }
  return 1;
}

void free_node (comp_tree_t* node) {
  int i;
  for (i=0; i < node->number_of_childs; i++) {
    if(node->childs[i] != NULL)
      free_node(node->childs[i]);
  }
  free (node);
  node = NULL;
  
}
