#include "misc.h"
#include "comp_list.h"
#include "comp_tree.h"
#include <string.h>
int getLineNumber (void)
{
  return linha;
}

void yyerror (char const *mensagem)
{
  fprintf (stderr, "linha %d: %s\n", getLineNumber(), mensagem); //altere para que apareça a linha
}

void main_init (int argc, char **argv)
{
  //implemente esta função com rotinas de inicialização, se necessário
  stack = create_stack();
  table = push(stack, create_hash_table(1024));
  
}

reg_list *reverse(reg_list *list)
{
  reg_list *new_list = NULL;
  while (list) {
   reg_list *next = list->next;
    list->next = new_list;
    new_list = list;
    list = next;
  }
  return new_list ;
}

void main_finalize (void)
{
  //free(NULL);
  //implemente esta função com rotinas de inicialização, se necessário
  //free_node(tree);
  //dump_table(table);
}

char* get_register() {
  static int x = 1; 
  char* reg = malloc(10);
  snprintf(reg, 10, "r%d", x);
  x++;
  return reg;
}

char* get_label() {
  static int y = 1; 
  char* label = malloc(10);
  snprintf(label, 10,"l%d", y);
  y++;
  return label;
}

char* create_instruction(char* opcode, int n, ...) {
  char *instruction = malloc(100);
  va_list valist;
  va_start(valist, n);
  if (n == 3) 
    snprintf(instruction, 100, "%s %s, %s => %s",opcode, va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));
  else if (n == 2)
    snprintf(instruction, 100, "%s %s => %s",opcode, va_arg(valist, char*), va_arg(valist, char*));
  else if (n == 1)
    snprintf(instruction, 100, "%s => %s",opcode, va_arg(valist, char*));  
  va_end(valist);
  return instruction;
}


char* create_instruction_generic(char* opcode, int n, ...) {

  char *instruction = malloc(100);

  va_list valist;
  va_start(valist, n);

  if(strcmp(opcode,"multI") == 0)
    snprintf(instruction, 100, "multI %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"add") == 0)
    snprintf(instruction, 100, "add %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"addI") == 0)
    snprintf(instruction, 100, "addI %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"store") == 0)  
    snprintf(instruction, 100, "store %s => %s", va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"jumpI") == 0)  
     snprintf(instruction, 100, "jumpI => %s",   va_arg(valist, char*));

  if(strcmp(opcode,"sub") == 0)  
     snprintf(instruction, 100, "sub %s,%s => %s",   va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"mult") == 0)
    snprintf(instruction, 100, "mult %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"div") == 0)
    snprintf(instruction, 100, "div %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

 if(strcmp(opcode,"rsubI") == 0)
    snprintf(instruction, 100, "rsubI %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"cbr") == 0)
    snprintf(instruction, 100, "cbr %s => %s, %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"and") == 0)
    snprintf(instruction, 100, "and %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"or") == 0)
    snprintf(instruction, 100, "or %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"cmp_EQ") == 0)
    snprintf(instruction, 100, "cmp_EQ %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));
  
  if(strcmp(opcode,"cmp_NE") == 0)
    snprintf(instruction, 100, "cmp_NE %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"cmp_LE") == 0)
    snprintf(instruction, 100, "cmp_LE %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"cmp_GE") == 0)
    snprintf(instruction, 100, "cmp_GE %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"cmp_LT") == 0)
    snprintf(instruction, 100, "cmp_LT %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"xorI") == 0)
    snprintf(instruction, 100, "xorI %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"cmp_GT") == 0)
    snprintf(instruction, 100, "cmp_GT %s,%s => %s", va_arg(valist, char*), va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"loadI") == 0)
     snprintf(instruction, 100, "loadI %s => %s;",  va_arg(valist, char*), va_arg(valist, char*));

  if(strcmp(opcode,"load") == 0)
     snprintf(instruction, 100, "load %s => %s", va_arg(valist, char*), va_arg(valist, char*));

              //instruction = create_instruction("store", 2, att_reg, $<node>3->temp);

  va_end(valist);
  return instruction;
}


char* set_label(char* label)
{
  char *instruction = malloc(100);
  snprintf(instruction, 100, "%s: nop",label);
  return instruction;
}
