#include "comp_list.h"
#include <string.h>
#include <stdlib.h>

comp_list_t* create_list() {
  comp_list_t* list = malloc(sizeof(comp_list_t));
  list->first = NULL;
  list->last = NULL;
  //if (list->first == NULL && list->last == NULL)
  return list;
}

comp_list_t* concat(comp_list_t* listA, comp_list_t* listB) {
  comp_list_t* list;// = malloc(sizeof(comp_list_t));
  if(listA->first == NULL && listB->first == NULL)
  {
    list = NULL;
    free(listA);
    free(listB);
    return list;
  }

  if(listA->first == NULL)
  {
    list = listB;
    free(listA);
    return list;
  }

  if(listB->first == NULL)
  {
    list = listA;
    free(listB);
    return list;
  }
  
  list = listA;
  if (listA->last != NULL) {
    list->last->next = listB->first;
    list->last = listB->last;
  } else { 
    list->last = listB->first;
  }
  return list;
}

comp_list_t* append(comp_list_t* list, char* instruction) {
  comp_list_item_t* list_item = malloc(sizeof(comp_list_item_t));
  list_item->next = NULL;
  list_item->instruction = malloc(strlen(instruction)+1);
  strcpy(list_item->instruction, instruction);
  if(list->first == NULL){
    list->first = list_item;
  }
  if (list->last == NULL) {
    list->last = list_item;
  } else {
    list->last->next = list_item;
    list->last = list_item;
  }
  return list;
}

comp_list_t* prepend(comp_list_t* list, char* instruction) {
  comp_list_item_t* list_item = malloc(sizeof(comp_list_item_t));
  list_item->next = NULL;
  list_item->instruction = malloc(strlen(instruction)+1);
  strcpy(list_item->instruction, instruction);
  if(list->last == NULL)
  {
    list->last = list_item;
  }
  if (list->first == NULL) {
    list->first = list_item;

  } else {
    list_item->next = list->first;
    list->first = list_item;
  }
  return list;
}

void print_list (comp_list_t* list) 
{
  comp_list_item_t* iterator;
  for (iterator = list->first; iterator != NULL; iterator = iterator->next) {
      puts(iterator->instruction);
  } 
}
