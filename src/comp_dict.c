#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "comp_dict.h"
#include "misc.h"
comp_dict_t* create_hash_table(int size)
{
    comp_dict_t *new_table;
    int i;
    if (size<1) 
	return 0; 


    if ((new_table = malloc(sizeof(comp_dict_t))) == 0) {
        return 0;
    }

    /* Attempt to allocate memory for the table itself */
    if ((new_table->table = malloc(sizeof(comp_dict_entry_t*) * size)) == 0) {
        return 0;
    }

    /*initialize elements*/
    for(i=0; i<size; i++) new_table->table[i] = 0;


    new_table->size = size;

    return new_table;
}

unsigned int hash(comp_dict_t *hashtable, char *key)
{
    unsigned int hashval;

    
    hashval = 0;

    /* hash generation */
    for(; *key != '\0'; key++) hashval = *key + (hashval << 5) - hashval;

   
    return hashval % hashtable->size;
}


comp_dict_entry_t* lookup_key(comp_dict_t* hashtable, char* key, int type)
{
    comp_dict_entry_t *data;
    unsigned int hashval = hash(hashtable, key);

    
    for(data = hashtable->table[hashval]; data != 0; data = data->next) {
      if (strcmp(key, data->key) == 0 && type == data->type)
        return data;
    }
    return NULL;
}

comp_dict_entry_t * add_data(comp_dict_t* hashtable, char* key, comp_dict_item_t data)
{
    comp_dict_entry_t *new_data;
    comp_dict_entry_t *current_data;
    unsigned int hashval = hash(hashtable, key);

    current_data = lookup_key(hashtable, key, data.type);
	
    //key already in the table
    if (current_data != NULL)
    {
		current_data->data = data;
		return current_data;
    }
    
	/* Attempt to allocate memory for compt_dict */
	if ((new_data = malloc(sizeof(comp_dict_entry_t))) == 0) 
		return NULL;
	/* Insert into list */
    new_data->key = strdup(key);
    new_data->type = data.type;
    new_data->data = data;
    new_data->next = hashtable->table[hashval];
    hashtable->table[hashval] = new_data;
    return hashtable->table[hashval];
}
/*
//compare number of args and types
int compare_arguments(argument_t *function_args, argument_t *call_args)
{	//printf("\ncompare args\n");
	argument_t *next_function = function_args;
	argument_t *next_call = call_args;

	while(next_function != NULL)
	{
		//types are diferent
		if(next_function->type != next_call->type)
		{
			return IKS_ERROR_WRONG_TYPE_ARGS;
		}
		//insuficient number of args
		if(next_call == NULL)
		{
			return IKS_ERROR_MISSING_ARGS;
		}
		next_call= next_call->next;		
		next_function= next_function->next;
	}
	//equal args list
	if(next_call == NULL)
		return 1;
	
	//more arguments than it should have
	return IKS_ERROR_EXCESS_ARGS;

}

//add argument type to the list of args
void add_argument_to_list(argument_t *args_list, int new_argument_type)
{			
	//printf("\nadd arg\n");
	argument_t *temp = malloc(sizeof(argument_t));
	temp->next = NULL;
	
	temp->type = new_argument_type;

	argument_t *next_function = args_list;
	//go to the last arg
	//printf("\nadd arg\n");
	while(next_function != NULL)
	{
        //printf("\nadd arg\n");
        if(next_function->next == NULL)
            break;
		
	    //printf("\nprinting arguments\n");
		next_function= next_function->next;
	}

	//attach the new arg
	next_function->next = temp;
	
}
*/
void dump_args(arg_list_t *args_list)
{
    
	//printf("%d,", args_list->type);			
    arg_list_t *next_function = args_list;

	if(next_function == NULL)
			printf(" NULL");
	while(next_function != NULL)
	{
		printf("%d,", next_function->symbol->type);
			if(next_function->next == NULL)
				break;
		
	
		next_function= next_function->next;
	}
    
}

void dump_table(comp_dict_t* hashtable) 
{
  int i;
  puts("========================================================");
  for (i = 0;i< hashtable->size; i++) {
    comp_dict_entry_t *data;
    for(data = hashtable->table[i]; data != 0; data = data->next) {
      printf("key: %s line: %d type: %d ", data->key, data->data.line, data->data.type);
      
      if(data->data.type == IKS_SIMBOLO_IDENTIFICADOR && data->data.id_type == ID_TYPE_FUNCTION)
      {	
          printf(" with arguments:"); dump_args(data->args_list);

      } 
      switch (data->data.type) {
        case 1:
            printf("value: %d\n", data->data.value.int_data);
          break; 
        case 2:
            printf("value: %f\n", data->data.value.float_data);
          break; 
        case 3:
            printf("value: %c\n", data->data.value.char_data);
          break; 
        case 4:
            printf("value: %s\n", data->data.value.string_data);
          break; 
        case 5:
            printf("value: %d\n", data->data.value.int_data);
          break; 
        default:
            printf("\n"); 
          break;
      }		
    }
  }
  puts("========================================================");
}
