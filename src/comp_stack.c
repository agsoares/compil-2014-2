#include "comp_stack.h"

comp_stack_t* create_stack() {
  comp_stack_t *stack = malloc(sizeof(comp_stack_t));
  stack->bottom = NULL;
  stack->top = NULL;
  return stack;
}

comp_dict_t* push(comp_stack_t *stack, comp_dict_t *value) {
  comp_stack_entry_t *item = malloc(sizeof(comp_stack_entry_t));
  item->prev = NULL;
  item->next = NULL;
  item->return_list = NULL;
  item->args_list   = NULL;
  item->value = value;
  item->memory_position_counter = 0;
  if (stack->bottom == NULL) {
    stack->bottom = item;
    stack->top = item;
  } else {
    item->prev = stack->top;
    stack->top = item;
  }
  return value;
}

comp_dict_t* pop(comp_stack_t *stack) {
  comp_stack_entry_t *old_value = stack->top;
  stack->top = stack->top->prev;
  
  return_list_t* iterator;
  return_list_t* prev = NULL;
  for (iterator = old_value->return_list; iterator != NULL ; iterator = iterator->next) {
    free(prev);
    prev = iterator;
  }
  free(prev);
  
  free(old_value);
  if (stack->top == NULL) {
    stack->bottom = NULL;
    return NULL;
  }
  return stack->top->value;
}

comp_dict_t* peek(comp_stack_t *stack) {
  if (stack->top != NULL) {
    return stack->top->value;
  } else {
    return NULL;
  }
}

//returns 1 if found the id on the stack, 0 if not
comp_dict_entry_t* search_for_id_stack(comp_stack_t *stack, char* key)
{
  comp_dict_entry_t *current_data;
  comp_stack_entry_t *current_stack_level = stack->top;
  while(current_stack_level != NULL)
  {
    current_data = lookup_key(current_stack_level->value, key, IKS_SIMBOLO_IDENTIFICADOR);

    //key already in the table
    if (current_data != NULL)
    {
       // printf("found");
        return current_data;
    }
    if(current_stack_level->prev == NULL)
    {
        break;
    }
    current_stack_level = current_stack_level->prev;
  }
  return NULL;
}


