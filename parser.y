/*
  Praise the Sun
	 -Adriano Soares
	 -Luiza Eitelvein
	 -Vinicius Herbstrith
*/
%error-verbose
%{
#include "comp_dict.h"
#include "comp_tree.h"
#include "misc.h"
#include "iks_ast.h"
#include "main.h"
#include <string.h>
#include <stdlib.h>
comp_dict_t* table;  
comp_tree_t* tree;
comp_tree_list_t* tree_list;
return_list_t* return_list = NULL;
  
int linha;
%}

%union {
  struct comp_tree_t* node;
  struct comp_dict_entry_t* symbol;
  struct comp_dict_item_t scanner_data;
  struct arg_list_t* args_list;
  int type;
  int size;
  char* str;

}

/* Declaração dos tokens da linguagem */
%token <type> TK_PR_INT
%token <type> TK_PR_FLOAT
%token <type> TK_PR_BOOL
%token <type> TK_PR_CHAR
%token <type> TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token <symbol> TK_LIT_INT
%token <symbol> TK_LIT_FLOAT
%token <symbol> TK_LIT_FALSE
%token <symbol> TK_LIT_TRUE
%token <symbol> TK_LIT_CHAR
%token <symbol> TK_LIT_STRING
%token <str> TK_IDENTIFICADOR
%token TOKEN_ERRO
  
%type <type> tipo  
%type <type> cabecalho_funcao  
%type <args_list> argumento  
%type <args_list> lista_argumentos
%type <size> lista_inteiros
  
/*
%type <node> programa
%type <node> codigo
%type <node> declaracao_funcao
%type <node> corpo_funcao
%type <node> bloco_comando

%type <node> identificador
%type <node> vetor_indexado

%type <node> sequencia_comandos
%type <node> comando
%type <node> comando_simples
%type <node> atribuicao

%type <node> expressao
%type <node> numero
%type <node> lista_expressao
%type <node> fluxo_controle
%type <node> chamada_funcao
*/

%right TK_PR_THEN TK_PR_ELSE
%left TK_OC_OR
%left TK_OC_AND
%left '<' '>' TK_OC_EQ TK_OC_NE TK_OC_LE TK_OC_GE
%left '-' '+'
%left '/' '*'
%right NEG
%nonassoc PAR



%%
/* Regras (e ações) da gramática */

programa: codigo  { 
                    if ($<node>1 != NULL) {
                      $<node>$ = create_node(IKS_AST_PROGRAMA, NULL, 1, $<node>1);  
                    } else {
                      $<node>$ = create_node(IKS_AST_PROGRAMA, NULL, 0, NULL);
                    }
                    tree = $<node>$; 
                  }
        |         { 
                    $<node>$ = create_node(IKS_AST_PROGRAMA, NULL, 0, NULL); 
                    tree = $<node>$; 
                  }
        ;

codigo: declaracao_global         { $<node>$ = NULL; }
      | declaracao_funcao         { $<node>$ = $<node>1; } 
      | declaracao_global codigo  { $<node>$ = $<node>2; }
      | declaracao_funcao codigo  { 
                                    if ($<node>2 != NULL) {
                                      $<node>$ = add_child($<node>1, $<node>2);
                                    } else {
                                      $<node>$ = $<node>1;
                                    }
                                  }
      ;

declaracao_global: tipo TK_IDENTIFICADOR  ';' {
                                                $<str>2[strlen($<str>2)-1] = 0;
                                                if(lookup_key(table, $<str>2, IKS_SIMBOLO_IDENTIFICADOR) != NULL) {
                                                  return IKS_ERROR_DECLARED;
                                                } else {
                                                  comp_dict_item_t data;
                                                  data.id_type = ID_TYPE_SIMPLE;
                                                  data.line = linha;
                                                  data.type = IKS_SIMBOLO_IDENTIFICADOR;
                                                  comp_dict_entry_t *dict_entry = add_data(table, $<str>2, data);
                                                  dict_entry->data.type = $1;
                                                  switch (dict_entry->data.type) {
                                                      case IKS_INT: dict_entry->data.size = 4;
                                                           break;
                                                      case IKS_FLOAT: dict_entry->data.size = 8;
                                                           break;
                                                      case IKS_CHAR: dict_entry->data.size = 1;
                                                           break;
                                                      case IKS_BOOL: dict_entry->data.size = 1;
                                                           break;
                                                      case IKS_STRING: dict_entry->data.size = 0;
                                                           break;
                                                  }
                                                 dict_entry->data.memory_position = stack->top->memory_position_counter;
                                                 stack->top->memory_position_counter += dict_entry->data.size;
                                                  dict_entry->data.global = 1;
                                                } /*verifica se identificador ja foi definido ou não na tabela topo*/
                                                
                                                
                                              }
                 | tipo TK_IDENTIFICADOR  '['lista_inteiros']' ';' {
                                             $<str>2[strlen($<str>2)-1] = 0;
                                             $<str>2[(strchr($<str>2, '[')-$<str>2)] = 0;
                                            if(lookup_key(table, $<str>2, IKS_SIMBOLO_IDENTIFICADOR) != NULL) {
                                                return IKS_ERROR_DECLARED;
                                            } else {
                                                comp_dict_item_t data;
                                                data.line = linha;
                                                data.id_type = ID_TYPE_ARRAY;
                                                data.type = IKS_SIMBOLO_IDENTIFICADOR; 
                                                comp_dict_entry_t *dict_entry = add_data(table, $<str>2, data);
                                                dict_entry->data.type = $1;
                                                dict_entry->data.size =  $<symbol>4->data.size;
                                                dict_entry->data.array_indexes_num =  $<symbol>4->data.array_indexes_num;
                                                //salva a ordem dos indexes do array invertido
                                                memcpy(dict_entry->data.array_indexes_size,$<symbol>4->data.array_indexes_size,sizeof($<symbol>4->data.array_indexes_size));
                                               // printf("\n%d\n",dict_entry->data.array_indexes_size[0]);
                                              //  dict_entry->data.array_indexes_size =  $<symbol>3->data.array_indexes_size;
                                                int size = $<symbol>4->data.size;
                                                switch (dict_entry->data.type) {
                                                    case IKS_INT: dict_entry->data.size = 4*size;
                                                         break;
                                                    case IKS_FLOAT: dict_entry->data.size = 8*size;
                                                         break;
                                                    case IKS_CHAR: dict_entry->data.size = 1*size;
                                                         break;
                                                    case IKS_BOOL: dict_entry->data.size = 1*size;
                                                         break;
                                                    case IKS_STRING: dict_entry->data.size = 0;
                                                         break;
                                                }
//printf("\n %d\n", dict_entry->data.size);
                                              dict_entry->data.memory_position = stack->top->memory_position_counter;
                                              stack->top->memory_position_counter += dict_entry->data.size;
//printf("\n pos %d\n", stack->top->memory_position_counter);
                                               dict_entry->data.global = 1;
                                            } /*verifica se identificador ja foi definido ou não na tabela topo*/
                                          }
                 ;

lista_inteiros: TK_LIT_INT { $<symbol>$->data.array_indexes_num=0;
                             $<symbol>$->data.size = $1->data.value.int_data;
                             $<symbol>$->data.array_indexes_size[$<symbol>$->data.array_indexes_num]=$1->data.value.int_data;
                             $<symbol>$->data.array_indexes_num++; 
                            }
              | TK_LIT_INT',' lista_inteiros { 
                                              $<symbol>$->data.size = $1->data.value.int_data * $<symbol>3->data.size;
                                              memcpy($<symbol>$->data.array_indexes_size,$<symbol>3->data.array_indexes_size,sizeof($<symbol>3->data.array_indexes_size));
                                              $<symbol>$->data.array_indexes_size[$<symbol>3->data.array_indexes_num]=$1->data.value.int_data;
                                              $<symbol>$->data.array_indexes_num =$<symbol>3->data.array_indexes_num+1 ;

                                             }
              ;

declaracao_funcao: cabecalho_funcao corpo_funcao  
                    { 
                      if($<node>2 != NULL) {  
                        $<node>$ = create_node(IKS_AST_FUNCAO, NULL, 1, $<node>2);
                        
                      } else {
                        $<node>$ = create_node(IKS_AST_FUNCAO, NULL, 0, NULL); 
                      }
                      $<node>$->iks_type = $1;
                      return_list_t* iterator;
                      return_list_t* prev = NULL;
                      $<node>$->symbol_table = table;
                      for (iterator = stack->top->return_list; iterator != NULL ; iterator = iterator->next) {
                        if ($<node>$->iks_type == IKS_CHAR && iterator->node->iks_type != IKS_CHAR)     return IKS_ERROR_WRONG_PAR_RETURN;
                        if ($<node>$->iks_type == IKS_STRING && iterator->node->iks_type != IKS_STRING) return IKS_ERROR_WRONG_PAR_RETURN;
                        if ($<node>$->iks_type != IKS_CHAR && iterator->node->iks_type == IKS_CHAR)     return IKS_ERROR_WRONG_PAR_RETURN;
                        if ($<node>$->iks_type != IKS_STRING && iterator->node->iks_type == IKS_STRING) return IKS_ERROR_WRONG_PAR_RETURN;
                        if ($<node>$->iks_type != iterator->node->iks_type) iterator->node->coercion_type = $<node>$->iks_type;
                      }
                      
                      //dump_args( $<node>$->args);
                      table = pop(stack);
                      //dump_table(stack->top->value);
                    }
                 ;

tipo: TK_PR_INT     { $$ = IKS_INT; }
    | TK_PR_FLOAT   { $$ = IKS_FLOAT; }
    | TK_PR_BOOL    { $$ = IKS_BOOL; }
    | TK_PR_CHAR    { $$ = IKS_CHAR; }
    | TK_PR_STRING  { $$ = IKS_STRING; }
    ;

cabecalho_funcao: tipo TK_IDENTIFICADOR '(' { 
                                              $<str>2[strlen($<str>2)-1] = 0;
                                              $<str>2[(strchr($<str>2, '(')-$<str>2)] = 0;
                                              if (lookup_key(table, $<str>2, IKS_SIMBOLO_IDENTIFICADOR) != NULL) {
                                                return IKS_ERROR_DECLARED;
                                              } else {
                                                comp_dict_item_t data;
                                                data.id_type = ID_TYPE_FUNCTION;
                                                data.line = linha;
                                                data.type = IKS_SIMBOLO_IDENTIFICADOR;
                                                data.id_type = ID_TYPE_FUNCTION; 
                                                comp_dict_entry_t *dict_entry = add_data(table, $<str>2, data);
                                                dict_entry->data.type = $1;
                                                table = push(stack, create_hash_table(1024)); 
                                              } /*verifica se identificador ja foi definido ou não na tabela topo*/
                                           }
                                           lista_argumentos  ')' { 
                                              $$ = $1; 
                                              comp_dict_entry_t *dict_entry = lookup_key(stack->top->prev->value, $<str>2, IKS_SIMBOLO_IDENTIFICADOR);
                                              dict_entry->args_list = stack->top->args_list;
                                           }
                | tipo TK_IDENTIFICADOR '(' ')'{
                                    $<str>2[strlen($<str>2)-1] = 0;
                                    $<str>2[(strchr($<str>2, '(')-$<str>2)] = 0;
                                    if(lookup_key(table, $<str>2, IKS_SIMBOLO_IDENTIFICADOR) != NULL) {
                                        return IKS_ERROR_DECLARED;
                                    } else {
                                        comp_dict_item_t data;
                                        data.id_type = ID_TYPE_FUNCTION;
                                        data.line = linha;
                                        data.type = IKS_SIMBOLO_IDENTIFICADOR;  
                                        comp_dict_entry_t *dict_entry = add_data(table, $<str>2, data);
                                        dict_entry->data.type = $1;
                                        $<type>$ = $1;
                                        dict_entry->args_list = NULL;
                                        table = push(stack, create_hash_table(1024)); 
                                    } /*verifica se identificador ja foi definido ou não na tabela topo*/
                                              }

corpo_funcao: '{' sequencia_comandos '}'  { $<node>$ = $<node>2; print_list($<node>2->code); }

bloco_comando: { table = push(stack, create_hash_table(1024));} 
               '{' sequencia_comandos '}' {
                                            if ($<node>3 != NULL) {
                                              $<node>$ = create_node(IKS_AST_BLOCO, NULL, 1, $<node>3); 
                                              $<node>$->code = concat($<node>$->code, $<node>3->code);
                                            } else {
                                              $<node>$ = create_node(IKS_AST_BLOCO, NULL, 0, NULL); 
                                              //$<node>$->code = create_list();
                                            }
                                            $<node>$->symbol_table = table;
                                            table = pop(stack);

                                            //print_list($<node>$->code);
                                          }
             ;

lista_argumentos: argumento ',' lista_argumentos { $1->next = stack->top->args_list; stack->top->args_list = $1; }
                | argumento { stack->top->args_list = $1; }
                ;

argumento: tipo TK_IDENTIFICADOR  {
                                      $<str>2[strlen($<str>2)-1] = 0;
                                      comp_dict_item_t data; 
                                      data.id_type = ID_TYPE_SIMPLE;
                                      data.line = linha;
                                      data.type = $1;                                  
                                      switch (data.type) {
                                            case IKS_INT: data.size = 4;
                                                 break;
                                            case IKS_FLOAT:data.size = 8;
                                                 break;
                                            case IKS_CHAR: data.size = 1;
                                                 break;
                                            case IKS_BOOL: data.size = 1;
                                                 break;
                                            case IKS_STRING:data.size = 0;
                                                 break;
                                      }
                                      data.memory_position = stack->top->memory_position_counter;
                                      stack->top->memory_position_counter += data.size;

                                      comp_dict_entry_t *dict_entry = add_data(table, $<str>2, data);
                                      dict_entry->data.type = $1;
                                      arg_list_t* arg = malloc(sizeof(arg_list_t));
                                      arg->next = NULL;
                                      arg->symbol = dict_entry;
                                      $$ = arg;
                                    }
          | tipo TK_IDENTIFICADOR '[' expressao ']' 
          { yyerror("Array não pode ser parametro de argumento de função"); yyerror;}
          | tipo TK_IDENTIFICADOR '[' ']'           
          { yyerror("Array não pode ser parametro de argumento de função"); yyerror;}
          ;

sequencia_comandos : ';' sequencia_comandos { $<node>$ = $<node>2; /* if ($<node>2 == NULL) $<node>$->code = create_list(); */ }
                   | comando_simples ';' sequencia_comandos  { 
                       if ($<node>1 == NULL) {
                         $<node>$ = $<node>3;
                       } else {
                         if ($<node>3 != NULL) {
                           $<node>$ = add_child($<node>1, $<node>3);
                           $<node>$->code = concat($<node>1->code, $<node>3->code);
                            
                         } else {
                           $<node>$ = $<node>1;
                         }
                       }
                     }
                   | comando_simples   { $<node>$ = $<node>1; }
                   |                   { $<node>$ = NULL; }                     
                   ;

comando: tipo TK_IDENTIFICADOR  { 
                                  if(lookup_key(table, $<str>2, IKS_SIMBOLO_IDENTIFICADOR)!=NULL)
                                  {
                                    return IKS_ERROR_DECLARED;
                                  }
                                  else{
                                      comp_dict_item_t data;
                                      data.id_type = ID_TYPE_SIMPLE;
                                      data.line = linha;
                                      data.type = IKS_SIMBOLO_IDENTIFICADOR;
                                      comp_dict_entry_t *dict_entry = malloc(sizeof(comp_dict_entry_t));
                                      dict_entry = add_data(table, $<str>2, data);
                                      dict_entry->data.type = $1;
                                      switch (dict_entry->data.type) {
                                          case IKS_INT: dict_entry->data.size = 4;
                                               break;
                                          case IKS_FLOAT: dict_entry->data.size = 8;
                                               break;
                                          case IKS_CHAR: dict_entry->data.size = 1;
                                               break;
                                          case IKS_BOOL: dict_entry->data.size = 1;
                                               break;
                                          case IKS_STRING: dict_entry->data.size = 1;
                                               break;
                                      }
                                      dict_entry->data.memory_position = stack->top->memory_position_counter;
                                      stack->top->memory_position_counter += dict_entry->data.size;
                                      dict_entry->data.global = 0;
                                            
                                  }
                                 $<node>$ = NULL;                                
                                  
                                }  
       | atribuicao             { $<node>$ = $<node>1; }
       | TK_PR_INPUT expressao     { 
           if ($<node>2->type != IKS_SIMBOLO_IDENTIFICADOR) return IKS_ERROR_WRONG_PAR_INPUT;
           $<node>$ = create_node(IKS_AST_INPUT, NULL, 1, $<node>2); 
         }
       | TK_PR_OUTPUT lista_output  { 
           $<node>$ = create_node(IKS_AST_OUTPUT, NULL, 1, $<node>2);
         }
       | TK_PR_RETURN expressao        { 
           $<node>$ = create_node(IKS_AST_RETURN, NULL, 1, $<node>2); 
           $<node>$->iks_type = $<node>2->iks_type;
           return_list_t* return_node = malloc(sizeof(return_list_t));
           return_node->node = $<node>$;
           return_node->next = stack->top->return_list;
           stack->top->return_list = return_node;
         }
       | fluxo_controle { $<node>$ = $<node>1; /*print_list($<node>1->code); */}
       | chamada_funcao { $<node>$ = $<node>1; }
       ; 

comando_simples: bloco_comando { $<node>$ = $<node>1; }
               | comando       { $<node>$ = $<node>1; }
               ;

identificador: TK_IDENTIFICADOR  {	        
                                    comp_dict_entry_t* symbol = search_for_id_stack(stack, $<str>1);
                                    if(symbol != NULL) {
                                        symbol->data.line = linha;
                                    } else {
                                        //printf("identificador %s não declarado linha %d",$<str>1, linha);
                                        return IKS_ERROR_UNDECLARED;
                                    }   /*buscar identificador nao declarado */ 
                                    
                                    $<node>$ = create_node(IKS_AST_IDENTIFICADOR, symbol, 0, NULL);
                                 }
             ;

vetor_indexado: identificador '['lista_expressao']' { 
                  int iks_type = $<node>1->symbol_table_entry->data.type; 
                /*  if ($<node>3->iks_type == IKS_CHAR)     return IKS_ERROR_CHAR_TO_X;
                  if ($<node>3->iks_type == IKS_STRING)   return IKS_ERROR_STRING_TO_X;
                  if ($<node>1->symbol_table_entry->data.id_type == ID_TYPE_SIMPLE) return IKS_ERROR_VARIABLE;
                  if ($<node>1->symbol_table_entry->data.id_type == ID_TYPE_FUNCTION) return IKS_ERROR_FUNCTION;
                  */$<node>$ = create_node(IKS_AST_VETOR_INDEXADO, NULL, 2, $<node>1, $<node>3);
                  $<node>$->iks_type = iks_type;
                  $<node>$ = $<node>3;
                  $<node>$->symbol_table_entry = malloc(sizeof(comp_dict_entry_t));
                  $<node>$->symbol_table_entry->data.global = 1;
                  $<node>$->symbol_table_entry->data.type = iks_type;
                  $<node>$->temp_list = $<node>3->temp_list;
                  

                   int size;
                  switch (iks_type) {
                      case IKS_INT: size = 4;
                           break;
                      case IKS_FLOAT: size = 8;
                           break;
                      case IKS_CHAR:size = 1;
                           break;
                      case IKS_BOOL: size = 1;
                           break;
                      case IKS_STRING: size = 0;
                           break;
                  }
                  reg_list *actual_reg;// = malloc(sizeof(reg_list));
                  actual_reg = reverse($<node>3->temp_list);
                  char *instruction;
                  char *soma_enderecos = actual_reg->reg;
                  char *temp_reg ;

                 // snprintf(instruction, 100, "add rbss,%s => %s",  $<node>1->temp_list->reg,att_reg);
                  if(actual_reg->next != NULL)
                  {
                    actual_reg = actual_reg->next;
                    int nk =0 ;
                    int i =0;
                    while(actual_reg != NULL)
                    {
                      temp_reg = get_register();

                      nk = $<node>1->symbol_table_entry->data.array_indexes_size[i] + nk;//printf("\n index size %d and then%d\n ", $<node>1->symbol_table_entry->data.array_indexes_size[1],nk);
                      char *nk_char = malloc(100);
                     snprintf(nk_char, 100, "%d",nk);
                      instruction = create_instruction_generic("multI", 3, temp_reg, nk_char, actual_reg->reg);
                     //snprintf(instruction, 100, "multI %s,%d => %s",actual_reg->reg,nk,temp_reg);
                      free(nk_char);

                      append($<node>$->code, instruction);                      free(instruction);

                      instruction = create_instruction_generic("add", 3, soma_enderecos, soma_enderecos, temp_reg);
                     // snprintf(instruction, 100, "add %s,%s => %s", temp_reg,soma_enderecos,soma_enderecos);
                      append($<node>$->code, instruction);                        free(instruction);
                      i++;
                      actual_reg = actual_reg->next;
                      free(temp_reg);

                      
                    }
                  }
                  temp_reg = get_register();
                  char *temp_size = malloc(100);
                  snprintf(temp_size, 100, "%d",size);
                  instruction = create_instruction_generic("multI", 3, temp_reg, temp_size, soma_enderecos);                      
                 // snprintf(instruction, 100, "multI %s,%d => %s", soma_enderecos,size, temp_reg);
                  free(temp_size);

                  append($<node>$->code, instruction);                      free(instruction);
                  $<node>$->temp = temp_reg;
              

                  $<node>$->code=concat($<node>$->code,$<node>3->code);

                  char *temp_position = malloc(100);
                  snprintf(temp_position, 100, "%d",$<node>1->symbol_table_entry->data.memory_position);
                  instruction = create_instruction_generic("addI", 3, temp_reg,temp_position, temp_reg);                     
                  free(temp_position);

                 // snprintf(instruction, 100, "addI %s,%d => %s",  temp_reg,$<node>1->symbol_table_entry->data.memory_position,temp_reg);
                  append($<node>$->code, instruction);                       free(instruction);

                }
              ;

atribuicao: identificador '=' expressao   { 
              int iks_type = $<node>1->symbol_table_entry->data.type; 
              if (iks_type != IKS_CHAR && $<node>3->iks_type == IKS_CHAR)     return IKS_ERROR_CHAR_TO_X;
              if (iks_type != IKS_STRING && $<node>3->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X;
              if (iks_type == IKS_CHAR && $<node>3->iks_type != IKS_CHAR)     return IKS_ERROR_WRONG_TYPE;
              if (iks_type == IKS_STRING && $<node>3->iks_type != IKS_STRING) return IKS_ERROR_WRONG_TYPE;
              if ($<node>1->symbol_table_entry->data.id_type == ID_TYPE_ARRAY) return IKS_ERROR_VECTOR;
              if ($<node>1->symbol_table_entry->data.id_type == ID_TYPE_FUNCTION) return IKS_ERROR_FUNCTION;
              if (iks_type !=  $<node>3->iks_type ) $<node>3->coercion_type = iks_type;
              if (iks_type == IKS_STRING && $<node>3->type == IKS_AST_LITERAL)
              {
                  $<node>1->symbol_table_entry->data.size = (int)strlen($<node>3->symbol_table_entry->data.value.string_data);
              }
              $<node>$ = create_node(IKS_AST_ATRIBUICAO, NULL, 2, $<node>1, $<node>3);
              $<node>$->code = concat($<node>$->code, $<node>3->code);

              char *instruction ;
              char *att_reg =  get_register();
              //endereço que a variavel esta salva
              if($<node>1->symbol_table_entry->data.global == 1)
              {
                char *temp_position = malloc(100);
                snprintf(temp_position, 100, "%d",$<node>1->symbol_table_entry->data.memory_position);
                instruction = create_instruction_generic("addI", 3, att_reg, temp_position, "rbss");    
                free(temp_position);                  
                //snprintf(instruction, 100, "addI rbss,%d => %s",  $<node>1->symbol_table_entry->data.memory_position,att_reg);
              }
              else
              {
                char *temp_position = malloc(100);
                snprintf(temp_position, 100, "%d",$<node>1->symbol_table_entry->data.memory_position);
                instruction = create_instruction_generic("addI", 3, att_reg,temp_position, "rarp");                      
                free(temp_position);                  
               // snprintf(instruction, 100, "addI rarp,%d => %s", $<node>1->symbol_table_entry->data.memory_position,att_reg);
              }

              append($<node>$->code, instruction);
              free(instruction);
              $<node>$->code = concat($<node>$->code, $<node>3->code);

              instruction = create_instruction("store", 2, att_reg, $<node>3->temp);
              append($<node>$->code, instruction); 
              free(att_reg);
              free(instruction);
            }
          | vetor_indexado '=' expressao  {
              int iks_type = $<node>1->symbol_table_entry->data.type; 
              if (iks_type != IKS_CHAR && $<node>3->iks_type == IKS_CHAR)     return IKS_ERROR_CHAR_TO_X;
              if (iks_type != IKS_STRING && $<node>3->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X;
              if (iks_type == IKS_CHAR && $<node>3->iks_type != IKS_CHAR)     return IKS_ERROR_WRONG_TYPE;
              if (iks_type == IKS_STRING && $<node>3->iks_type != IKS_STRING) return IKS_ERROR_WRONG_TYPE;
              $<node>$ = create_node(IKS_AST_ATRIBUICAO, NULL, 2, $<node>1, $<node>3);

              //$<node>$->code = $<node>3->code;
              $<node>$->code = concat($<node>$->code, concat($<node>3->code, $<node>1->code));
              char *instruction;
              char *att_reg =  get_register();
              //endereço que a variavel esta salva
              if($<node>1->symbol_table_entry->data.global == 1)
              {
                
                instruction = create_instruction_generic("add", 3, att_reg,$<node>1->temp, "rbss");          
               // snprintf(instruction, 100, "add rbss,%s => %s",  $<node>1->temp,att_reg);
              }
              else
              {
                char *temp_position = malloc(100);
                snprintf(temp_position, 100, "%d",$<node>1->symbol_table_entry->data.memory_position);
                instruction = create_instruction_generic("addI", 3, att_reg,temp_position, "rarp");                      
                free(temp_position);                  
               // snprintf(instruction, 100, "addI rarp,%d => %s", $<node>1->symbol_table_entry->data.memory_position,att_reg);
              }

              append($<node>$->code, instruction);
              free(instruction);
              $<node>$->code = concat($<node>$->code, $<node>3->code);

              instruction = create_instruction("store", 2, att_reg,$<node>3->temp);
              append($<node>$->code, instruction); 
              free(att_reg);
              free(instruction);
            }
          ;


expressao: '(' expressao ')' %prec PAR { $<node>$ = $<node>2; }
         | expressao '+' expressao { 
             $<node>$ = create_node(IKS_AST_ARIM_SOMA, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)      return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
               $<node>$->iks_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
               $<node>$->iks_type = IKS_INT;
             } else {
               $<node>$->iks_type = IKS_BOOL;
             }
             $<node>$->temp = get_register();

             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));

        
              
             char *instruction = create_instruction("add", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
             free(instruction);
         }
         | expressao '-' expressao { 
             $<node>$ = create_node(IKS_AST_ARIM_SUBTRACAO, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
               $<node>$->iks_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
               $<node>$->iks_type = IKS_INT;
             } else {
               $<node>$->iks_type = IKS_BOOL;
             }
             $<node>$->temp = get_register();
             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
             char *instruction = create_instruction("sub", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
             free(instruction);
         }
         | expressao '*' expressao { 
             $<node>$ = create_node(IKS_AST_ARIM_MULTIPLICACAO, NULL, 2, $<node>1, $<node>3); 
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
               $<node>$->iks_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
               $<node>$->iks_type = IKS_INT;
             } else {
               $<node>$->iks_type = IKS_BOOL;
             }
             $<node>$->temp = get_register();
             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
             char *instruction = create_instruction("mult", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
             free(instruction);
           }
         | expressao '/' expressao { 
             $<node>$ = create_node(IKS_AST_ARIM_DIVISAO, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
               $<node>$->iks_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
               $<node>$->iks_type = IKS_INT;
             } else {
               $<node>$->iks_type = IKS_BOOL;
             }
             $<node>$->temp = get_register();
             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
             char *instruction = create_instruction("div", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
             free(instruction);
           }
         | '-' expressao %prec NEG { 
             $<node>$ = create_node(IKS_AST_ARIM_INVERSAO, NULL, 2, $<node>2);
             if ($<node>2->iks_type == IKS_CHAR) return IKS_ERROR_CHAR_TO_X;
             if ($<node>2->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X;
             if ($<node>2->iks_type == IKS_BOOL) {
               $<node>2->coercion_type = IKS_INT;
               $<node>$->iks_type = IKS_INT;
             } else {
               $<node>$->iks_type = $<node>2->iks_type;
             }
             $<node>$->temp = get_register();
             $<node>$->code = $<node>2->code;
             char *instruction = create_instruction("rsubI", 3, $<node>$->temp, "0", $<node>2->temp);
             append($<node>$->code, instruction);
             free(instruction);
           }
         | numero { $<node>$ = $<node>1;}

         | expressao TK_OC_AND expressao   { 
             $<node>$ = create_node(IKS_AST_LOGICO_E, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type != IKS_BOOL) $<node>1->coercion_type = IKS_BOOL;
             if ($<node>3->iks_type != IKS_BOOL) $<node>3->coercion_type = IKS_BOOL;
             $<node>$->iks_type = IKS_BOOL;
             //$<node>$->temp = get_register();
             $<node>$->temp = $<node>1->temp;

             char *labelFalse   = get_label();
             char *labelTrue  = get_label();
             char *instruction ;
              

             
                instruction = create_instruction_generic("cbr", 3, labelFalse,labelTrue, $<node>1->temp);                      

             //snprintf(instruction, 100, "cbr %s => %s, %s", $<node>1->temp, labelTrue, labelFalse);
             append($<node>1->code, instruction);free(instruction);

             instruction = set_label(labelTrue);
             append($<node>1->code, instruction);


             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
 
             instruction = create_instruction("and", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
 
              
             instruction = set_label(labelFalse);
             append($<node>$->code, instruction);
            
       
             free(instruction);
             free(labelTrue);
             free(labelFalse);

           }
         | expressao TK_OC_OR expressao    { 
             $<node>$ = create_node(IKS_AST_LOGICO_OU, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type != IKS_BOOL) $<node>1->coercion_type = IKS_BOOL;
             if ($<node>3->iks_type != IKS_BOOL) $<node>3->coercion_type = IKS_BOOL;
             $<node>$->iks_type = IKS_BOOL;
            // $<node>$->temp = get_register();
             $<node>$->temp = $<node>1->temp;


             char *labelFalse   = get_label();
             char *labelTrue  = get_label();
             char *instruction;
            



             instruction = create_instruction_generic("cbr", 3, labelFalse,labelTrue, $<node>1->temp);                      

             //snprintf(instruction, 100, "cbr %s => %s, %s", $<node>1->temp, labelTrue, labelFalse);
             append($<node>1->code, instruction);free(instruction);free(instruction);

             instruction = set_label(labelFalse);
             append($<node>1->code, instruction);


             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
 
             instruction = create_instruction("or", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
 


              
             instruction = set_label(labelTrue);
             append($<node>$->code, instruction);
            
       
             free(instruction);
             free(labelTrue);
             free(labelFalse);
           
           }
         | expressao TK_OC_EQ expressao { 
             $<node>$ = create_node(IKS_AST_LOGICO_COMP_IGUAL, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR) {
               if ($<node>1->iks_type != IKS_CHAR ) return IKS_ERROR_CHAR_TO_X;
               if ($<node>3->iks_type != IKS_CHAR ) return IKS_ERROR_CHAR_TO_X;
             }
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING) {
               if ($<node>1->iks_type != IKS_STRING ) return IKS_ERROR_STRING_TO_X;
               if ($<node>3->iks_type != IKS_STRING ) return IKS_ERROR_STRING_TO_X;
             } 
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
             }
             $<node>$->iks_type = IKS_BOOL;
             $<node>$->temp = get_register();
             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
             char *instruction = create_instruction("cmp_EQ", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
           }
         | expressao TK_OC_NE expressao    { 
             $<node>$ = create_node(IKS_AST_LOGICO_COMP_DIF, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR) {
               if ($<node>1->iks_type != IKS_CHAR ) return IKS_ERROR_CHAR_TO_X;
               if ($<node>3->iks_type != IKS_CHAR ) return IKS_ERROR_CHAR_TO_X;
             }
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING) {
               if ($<node>1->iks_type != IKS_STRING ) return IKS_ERROR_STRING_TO_X;
               if ($<node>3->iks_type != IKS_STRING ) return IKS_ERROR_STRING_TO_X;
             } 
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
             }
             $<node>$->iks_type = IKS_BOOL;
             $<node>$->temp = get_register();
             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
             char *instruction = create_instruction("cmp_NE", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
             free(instruction);
           }
         | expressao TK_OC_LE expressao    { 
             $<node>$ = create_node(IKS_AST_LOGICO_COMP_LE, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
             }
             $<node>$->iks_type = IKS_BOOL;
             $<node>$->temp = get_register();
             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
             char *instruction = create_instruction("cmp_LE", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
             free(instruction);
           }
         | expressao TK_OC_GE expressao    { 
             $<node>$ = create_node(IKS_AST_LOGICO_COMP_GE, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
             }
             $<node>$->iks_type = IKS_BOOL;
             $<node>$->temp = get_register();
             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
             char *instruction = create_instruction("cmp_GE", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
           }
         | '!' expressao %prec NEG { 
             $<node>$ = create_node(IKS_AST_LOGICO_COMP_NEGACAO, NULL, 1, $<node>2); 
             if ($<node>2->iks_type == IKS_CHAR) return IKS_ERROR_CHAR_TO_X;
             if ($<node>2->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X;
             if ($<node>2->iks_type != IKS_BOOL) $<node>2->coercion_type = IKS_BOOL;
             $<node>$->iks_type = IKS_BOOL;
             $<node>$->temp = get_register();
             $<node>$->code = $<node>2->code;
             char *instruction = create_instruction("xorI", 3, $<node>$->temp, "1", $<node>2->temp);
             append($<node>$->code, instruction);
           }
         | expressao '<' expressao { 
             $<node>$ = create_node(IKS_AST_LOGICO_COMP_L, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
             }
             $<node>$->iks_type = IKS_BOOL;
             $<node>$->temp = get_register();
             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
             char *instruction = create_instruction("cmp_LT", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
             free (instruction);
           }
         | expressao '>' expressao { 
             $<node>$ = create_node(IKS_AST_LOGICO_COMP_G, NULL, 2, $<node>1, $<node>3);
             if ($<node>1->iks_type == IKS_CHAR || $<node>3->iks_type == IKS_CHAR)  return IKS_ERROR_CHAR_TO_X;
             if ($<node>1->iks_type == IKS_STRING || $<node>3->iks_type == IKS_STRING)  return IKS_ERROR_STRING_TO_X;
             if ($<node>1->iks_type == IKS_FLOAT || $<node>3->iks_type == IKS_FLOAT) {
               if ($<node>1->iks_type != IKS_FLOAT) $<node>1->coercion_type = IKS_FLOAT;
               if ($<node>3->iks_type != IKS_FLOAT) $<node>3->coercion_type = IKS_FLOAT;
             } else if ($<node>1->iks_type == IKS_INT || $<node>3->iks_type == IKS_INT) {
               if ($<node>1->iks_type != IKS_INT) $<node>1->coercion_type = IKS_INT;
               if ($<node>3->iks_type != IKS_INT) $<node>3->coercion_type = IKS_INT;
             }
             $<node>$->iks_type = IKS_BOOL;
             $<node>$->temp = get_register();
             $<node>$->code = concat($<node>$->code, concat($<node>1->code, $<node>3->code));
             char *instruction = create_instruction("cmp_GT", 3, $<node>$->temp, $<node>3->temp, $<node>1->temp);
             append($<node>$->code, instruction);
             free(instruction);
           }
         | TK_LIT_TRUE { 
             $<node>$ = create_node(IKS_AST_LITERAL, $1, 0, NULL);
             $<node>$->iks_type = IKS_BOOL;
             $<node>$->temp = get_register();
             //$<node>$->code = create_list();
             char *instruction;

             instruction = create_instruction_generic("loadI", 2, $<node>$->temp, "1");                      

             //snprintf(instruction, 100, "loadI %d => %s;", 1, $<node>$->temp);;
             append($<node>$->code, instruction);free(instruction);
           }
         | TK_LIT_FALSE { 
             $<node>$ = create_node(IKS_AST_LITERAL, $1, 0, NULL);
             $<node>$->iks_type = IKS_BOOL;
             $<node>$->temp = get_register();
             //$<node>$->code = create_list();
             char *instruction;
             instruction = create_instruction_generic("loadI", 2, $<node>$->temp, "0");                      
             //snprintf(instruction, 100, "loadI %d => %s", 0, $<node>$->temp);;
             append($<node>$->code, instruction);free(instruction);
           }

         | TK_LIT_CHAR { 
             $<node>$ = create_node(IKS_AST_LITERAL, $1, 0, NULL);
             $<node>$->iks_type = IKS_CHAR;
           }
         | TK_LIT_STRING { 
             $<node>$ = create_node(IKS_AST_LITERAL, $1, 0, NULL);
             $<node>$->iks_type = IKS_STRING;
           }
         | identificador  { 
              $<node>$ = $<node>1; $<node>$->iks_type = $<node>$->symbol_table_entry->data.type;  
              char *instruction;
              char *load_position =  get_register();
              $<node>$->temp=  get_register(); 
             if($<node>1->symbol_table_entry->data.global == 1)
              {
                char *temp_position = malloc(100);
                snprintf(temp_position,100,"%d", $<node>1->symbol_table_entry->data.memory_position);
                
                instruction = create_instruction_generic("addI",3, load_position,temp_position, "rbss"); 
                free(temp_position);                     
                // snprintf(instruction, 100, "addI rbss,%d => %s",  $<node>1->symbol_table_entry->data.memory_position, load_position);
              }
             else
              {
                char *temp_position = malloc(100);
                snprintf(temp_position,100,"%d", $<node>1->symbol_table_entry->data.memory_position);
                
                instruction = create_instruction_generic("addI",3, load_position,temp_position, "rarp"); 
                free(temp_position); 
              // snprintf(instruction, 100, "addI rarp,%d => %s", $<node>1->symbol_table_entry->data.memory_position, load_position);
              }
             append($<node>$->code, instruction); free(instruction);

             instruction = create_instruction_generic("load",2, $<node>$->temp,load_position); 
             //snprintf(instruction, 100, "load %s => %s", load_position,$<node>$->temp);
             append($<node>$->code, instruction);
             free(load_position);
             free(instruction);
           }
         | vetor_indexado { 
            $<node>$ = $<node>1; 
              int iks_type = $<node>1->symbol_table_entry->data.type; 
              /*if (iks_type != IKS_CHAR && $<node>3->iks_type == IKS_CHAR)     return IKS_ERROR_CHAR_TO_X;
              if (iks_type != IKS_STRING && $<node>3->iks_type == IKS_STRING) return IKS_ERROR_STRING_TO_X;
              if (iks_type == IKS_CHAR && $<node>3->iks_type != IKS_CHAR)     return IKS_ERROR_WRONG_TYPE;
              if (iks_type == IKS_STRING && $<node>3->iks_type != IKS_STRING) return IKS_ERROR_WRONG_TYPE;*/
              //$<node>$ = create_node(IKS_AST_ATRIBUICAO, NULL, 2, $<node>1, $<node>3);

              $<node>$->code = $<node>1->code;
              $<node>$->code = concat($<node>$->code, $<node>1->code);
              char *instruction;
              char *load_position =  get_register();
              if($<node>1->symbol_table_entry->data.global == 1)
              {        
                instruction = create_instruction_generic("add",3, load_position,$<node>1->temp, "rbss"); 
               // snprintf(instruction, 100, "add rbss,%s => %s",  $<node>1->temp,load_position);
              }
              else
              {
                char *temp_position = malloc(100);
                snprintf(temp_position,100,"%d", $<node>1->symbol_table_entry->data.memory_position);
                
                instruction = create_instruction_generic("addI",3, load_position,temp_position, "rarp"); 
                free(temp_position); 
                //snprintf(instruction, 100, "addI rarp,%d => %s", $<node>1->symbol_table_entry->data.memory_position,load_position);
              }
              append($<node>$->code, instruction);
              free(instruction);
              instruction = create_instruction("load", 2,$<node>$->temp,load_position);
              append($<node>$->code, instruction); 
              free(load_position);
              free(instruction);
            }
         | chamada_funcao { $<node>$ = $<node>1; }
         ;

numero: TK_LIT_INT { 
          $<node>$ = create_node(IKS_AST_LITERAL, $1, 0, NULL);
          $<node>$->iks_type = IKS_INT;
          $<node>$->temp = get_register();
          //$<node>$->code = create_list();
          char *instruction=malloc(100);
          
          snprintf(instruction, 100, "loadI %d => %s", $1->data.value.int_data, $<node>$->temp);

          append($<node>$->code, instruction);
          free(instruction);
        }
      | TK_LIT_FLOAT { 
          $<node>$ = create_node(IKS_AST_LITERAL, $1, 0, NULL);
          $<node>$->iks_type = IKS_FLOAT;
          $<node>$->temp = get_register();
          //$<node>$->code = create_list();
          char *instruction=malloc(100);
          
          snprintf(instruction, 100, "loadI %f => %s", $1->data.value.float_data, $<node>$->temp);;
          append($<node>$->code, instruction);
          free(instruction);
        }
      ;


lista_output: expressao ',' lista_output { 
                   $<node>$ = add_child($<node>1, $<node>3);
                 }
               | expressao { 
                 $<node>$ = $<node>1;
                 if($<node>$->iks_type == IKS_CHAR) return IKS_ERROR_WRONG_PAR_OUTPUT; 
               }
//multiplica temp da expressao pelo tamanho do tipo e soma com a lista expressao
lista_expressao: expressao ',' lista_expressao {
                 $<node>$ = add_child($<node>1, $<node>3);
                 $<node>$->code=concat($<node>1->code,$<node>3->code);
                 $<node>$->temp_list = malloc(sizeof(reg_list));
                 $<node>$->temp_list->reg=$<node>1->temp;
                 $<node>$->temp_list->next=$<node>3->temp_list;                       
                 }
               | expressao { 
                 $<node>$ = $<node>1;
                 $<node>$->code=$<node>1->code;
                 reg_list *temp_list = malloc(sizeof(reg_list));
                 temp_list->reg=$<node>1->temp;                                  
                 $<node>$->temp_list=temp_list;
                 $<node>$->temp_list->next = NULL;
               } 
               ;//lista expressao retorna um nodo e os seus filhos são a lista dos tipos de argumentos... então vamos comparar a lista dos filhos(primeiro nivel) de lista expressão com os argumentos que foram encontrados.

fluxo_controle: TK_PR_IF '(' expressao ')' TK_PR_THEN comando_simples {                  
                  char *labelIF   = get_label();
                  char *labelEND  = get_label();
                  char *instruction  ;
                  instruction = create_instruction_generic("cbr", 3, labelEND,labelIF, $<node>3->temp);                      
                  //snprintf(instruction, 100, "cbr %s => %s, %s", $<node>3->temp, labelIF, labelEND);
                  append($<node>3->code, instruction);
                  free(instruction);
                  
                  instruction = set_label(labelIF);
                  prepend($<node>6->code, instruction);
                  free(instruction);
                  instruction = set_label(labelEND);
                  append($<node>6->code,instruction);
                  
                  $<node>$ = create_node(IKS_AST_IF_ELSE, NULL, 2, $<node>3, $<node>6);
                  $<node>$->code = concat($<node>$->code, concat($<node>3->code, $<node>6->code));
  
                  
                  free(labelIF); 
                  free(labelEND); 
                  free(instruction);
                  }
              | TK_PR_IF '(' expressao ')' TK_PR_THEN comando_simples 
                  TK_PR_ELSE comando_simples { 
                  char *labelIF   = get_label();
                  char *labelELSE = get_label();
                  char *labelEND  = get_label();

                  char *instruction;
                  instruction = create_instruction_generic("cbr", 3, labelELSE,labelIF, $<node>3->temp);                      
                  //snprintf(instruction, 100, "cbr %s => %s, %s", $<node>3->temp, labelIF, labelELSE);
                  append($<node>3->code, instruction);free(instruction);
                  instruction = set_label(labelIF);
                  prepend($<node>6->code, instruction);
                  instruction = create_instruction_generic("jumpI", 1, labelEND);                      
                  //snprintf(instruction, 100, "jumpI => %s",  labelEND);
                  append($<node>6->code, instruction);free(instruction);
                  instruction = set_label(labelELSE);
                  prepend($<node>8->code, instruction);
                  instruction = set_label(labelEND);
                  append($<node>8->code, instruction);
                  
                  $<node>$ = create_node(IKS_AST_IF_ELSE, NULL, 3, $<node>3, $<node>6, $<node>8); 
                  $<node>$->code = concat(concat($<node>3->code, $<node>6->code),$<node>8->code);
                  
                  free(labelIF); 
                  free(labelELSE); 
                  free(labelEND); 
                  free(instruction);
              }
              | TK_PR_WHILE '(' expressao ')' TK_PR_DO comando_simples { 
                  //jump para depois do comando_simples/ append jump de volta para o while no fim do comando_simples
                  char *labelINIT  = get_label();
                  char *labelLOOP  = get_label();
                  char *labelEND   = get_label();
                  char *instruction;
                  instruction = set_label(labelLOOP);
                  prepend($<node>3->code, instruction);free(instruction);
                  instruction = create_instruction_generic("cbr", 3, labelEND,labelINIT, $<node>3->temp);                      
                  //snprintf(instruction, 100, "cbr %s => %s, %s", $<node>3->temp, labelINIT, labelEND);
                  append($<node>3->code, instruction);
                  free(instruction);
                  instruction = set_label(labelINIT);
                  prepend($<node>6->code, instruction);free(instruction);
                  instruction = create_instruction_generic("jumpI", 1, labelLOOP);                      
                  //snprintf(instruction, 100, "jumpI => %s",  labelLOOP);
                  append($<node>6->code, instruction);
                
                  free(instruction);
                  instruction = set_label(labelEND);
                  append($<node>6->code,instruction);
                  
                  $<node>$ = create_node(IKS_AST_WHILE_DO, NULL, 2, $<node>3, $<node>6);
                  $<node>$->code = concat($<node>$->code, concat($<node>3->code,$<node>6->code));
                  free(labelINIT); 
                  free(labelLOOP); 
                  free(labelEND);
                  free(instruction);
                }
              | TK_PR_DO comando_simples TK_PR_WHILE '(' expressao ')' { 

                  char *labelLOOP  = get_label();
                  char *labelEND   = get_label();
                  char *instruction;
                  instruction = set_label(labelLOOP);
                  prepend($<node>2->code, instruction);
                  instruction = create_instruction_generic("cbr", 3, labelEND,labelLOOP, $<node>5->temp);                                        
                  //snprintf(instruction, 100, "cbr %s => %s, %s", $<node>5->temp, labelLOOP, labelEND);
                  append($<node>5->code,instruction);free(instruction);
                      
                  instruction = set_label(labelEND);
                  append($<node>5->code, instruction);
                
                  $<node>$ = create_node(IKS_AST_DO_WHILE, NULL, 2, $<node>2, $<node>5);
                  $<node>$->code = concat($<node>$->code, concat($<node>2->code, $<node>5->code));
                  //jump para o inicio do comando simples  
                  free(labelLOOP); 
                  free(labelEND); 
                  free(instruction);
                }
              | TK_PR_DO comando_simples ';' TK_PR_WHILE '(' expressao ')'  
                { yyerror("; inesperado"); yyerror;}
              ;
              
chamada_funcao: identificador '(' { $<str>2[strlen($<str>2)-1] = 0;$<str>2[(strchr($<str>2, '(')-$<str>2)] = 0;} 
                lista_expressao ')' { 
                        $<node>$ = create_node(IKS_AST_CHAMADA_DE_FUNCAO, NULL, 2, $<node>1, $<node>3); 
                        $<node>$->iks_type = $<node>1->symbol_table_entry->data.type;                       
                        comp_dict_entry_t* symbol = search_for_id_stack(stack, $<str>2);
                        if(symbol != NULL) {
                          if(symbol->args_list == NULL)
                            return IKS_ERROR_EXCESS_ARGS;
                         
                          int compare_result = compare_node_and_args($<node>4,symbol->args_list);
                          if(compare_result != 1)
                          {
                            return compare_result;
                          }
                          
                        }                      
                    }
              | identificador '(' ')' {
                        $<str>2[strlen($<str>2)-2] = 0;
                        comp_dict_entry_t* symbol = search_for_id_stack(stack, $<str>2);                
                        
                        if(symbol != NULL) {	
                          if(symbol->args_list != NULL)
                            return IKS_ERROR_MISSING_ARGS;
                        }
                        $<node>$ = create_node(IKS_AST_CHAMADA_DE_FUNCAO, NULL, 1, $<node>1); 
                        $<node>$->iks_type = $<node>1->symbol_table_entry->data.type;
                }
              ;


%%
